# importing the randint function to generate a random number between 2 given values
import random


#prompting users to enter their names
#assigning the user input to a variable named user_name
#converting all responses to lowercase
user_name = input("Hi there! What's your name? ").lower()

#counter for guesses
attempt = 5

#computer guesses your birthyear
for i in range(5):
  #randomly generating birth months
  birth_month = random.randint(1,12)
  
  #randomly generating birth years
  birth_year = random.randint(1924,2004)

  #we ask the player if we guessed it correct
  birth_year_guess = input(f"Guess {i} : {user_name.title()} were you born in {birth_month}/{birth_year}?").lower()
  
  #if we guessed it right we print the indented message
  if birth_year_guess == "yes":
    print("I knew it!😆")
    #we end the game
    break
  
  #if we guessed it wrong we print the indented message and deduct to the counter
  elif birth_year_guess == "no" and attempt != 0:
    print("Drat! Lemme try again!")
    attempt -= 1
  
  #if the player has guessed wrong and has no more chances, we say goodbye
  elif birth_year_guess == "no" and attempt == 0:
    print("I have other things to do. Good bye!")

  # if the player didn't entered either yes or no we tell them to enter valid inputs
  else:
    print("Your response is invalid! Please only enter 'yes' or 'no'.")
